ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    # Row 1
    columns do
      column do
        panel "Recent Changes" do
          table_for PaperTrail::Version.where("item_type='Publication'").order('id desc').limit(10).each do
            column("Item")   {|pt| pt.item.to_s }
            column("Type"){ |pt| pt.item_type.underscore.humanize}
            column("Modified at")   {|pt| pt.created_at.to_s :long }
            column ("Admin") { |pt|
              #Ensure it doesn't throw errors for existing trails without users
              pt.whodunnit? ? link_to(AdminUser.find(pt.whodunnit).email,
                                      [:admin, AdminUser.find(pt.whodunnit)]) : ""
            }
          end
        end
      end
    end

    # Row 2
    columns do

      column do
        panel "Recent Publications" do
          table_for Publication.order('id desc').limit(5).each do
            column("Id") {|publication| publication.id}
            column("Name")   {|publication| publication.name }
            column("Type"){ |publication| publication.type}
            column("Uri")   {|publication| publication.uri }
          end
        end
      end # End of column 1

      column do
        panel "Recent Sections" do
          table_for Section.order('id desc').limit(5).each do
            column("Name")   {|section| section.name }
            column("Publication") {|section| section.publication_id}
            column("Type"){ |section| section.asset_id}
            column("Uri")   {|section| section.color }
          end
        end
      end # End of column 2

    end # End of columns
  end # content

end # activeadmin
