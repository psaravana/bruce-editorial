ActiveAdmin.register Section do

  # To hide sections from the menu bar if required
  # menu :if => proc { false }

  # Permitted params for section
  permit_params :name, :asset_id, :image, :color, :publication_id, :position

  belongs_to :publication, :optional => true

  # assumes you are using 'position' for your acts_as_list column
  config.sort_order = 'position_asc'

    controller do

      # Apply pagination for sections page
      before_filter :only => :index do
        @per_page = 25
      end

      def new
        super
      end
      def show
        super
      end
    end

    form do |section_form|
      # To display errors at the top of the form
      section_form.semantic_errors *section_form.object.errors.keys

      # Form elements
      section_form.inputs do
        section_form.input :name
        section_form.input :asset_id
        section_form.input :image
        section_form.input :color
        section_form.input :position
        section_form.input :publication
      end

      # Form actions
      section_form.actions
    end

    index do |section|

      # Index for sections page
      if params[:publication_id].nil?
        column "Name", :name
        column "Publication", :publication_id
        column "Asset Id", :asset_id
        column "Color", :color
        column "Image", :image
        section.actions
      else
        # Panel to display publication details
        panel "Publication", 'id' => 'publication', 'data-publication-id' => publication.id do
          columns do

            column do
              h3 do
                b "Name"
              end
              h4 publication.name
            end

            column do
              h3 do
                b "Type"
              end
              h4 publication.type
            end

            column do
              h3 do
                b "URI"
              end
              h4 publication.uri
            end

            column do
              h3 do
                b "API"
              end
              h4 link_to "json preview", "#{publication.uri}", target: '_blank'
            end

          end
        end
        # index_table for section with drag handle as first column
        # glyphicon glyphicon-sort pointer
        column '', :class => "activeadmin-sortable " do |resource|
          sort_url, query_params = resource_path(resource).split '?', 2
          sort_url += "/sort"
          sort_url += "?" + query_params if query_params
          content_tag :span, '&#x2195;'.html_safe, :class => 'handle', 'data-sort-url' => sort_url
        end
        column "Name", :name
        column "Asset Id", :asset_id
        column "Color", :color
        column "Image", :image
      end
    end

end
