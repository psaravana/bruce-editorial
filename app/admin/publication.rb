ActiveAdmin.register Publication do

    # Permitted params for publication
    permit_params :name, :type, :uri, sections_attributes: [:id, :name, :asset_id, :image, :color, :position, :_destroy]

    controller do
      def new
          super
      end

      def show
        redirect_to "/admin/publications/#{params[:id]}/sections/"
      end
    end

    form do |publication_form|
      # To display errors at the top of the form
      publication_form.semantic_errors *publication_form.object.errors.keys

      # Publications
      publication_form.inputs do
        publication_form.input :name
        publication_form.input :type, :as => :select, collection: Publication.types.keys
        publication_form.input :uri, :input_html => { :readonly => true }
      end

      # Sections
      publication_form.inputs "Sections" do

        publication_form.has_many :sections, :allow_destroy => true, :heading => 'Section' do |section_form|
          section_form.input :name
          section_form.input :asset_id
          section_form.input :image
          section_form.input :color
          section_form.input :position
        end
      end

      # Form actions
      publication_form.actions

    end

    index do |publication_form|
      column :name
      column :type
      column "URI" do |publication|
        link_to "#{publication.uri}", "#{publication.uri}", target: '_blank'
      end
      # Form actions
      publication_form.actions
    end
end
