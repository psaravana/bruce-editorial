class Section < ActiveRecord::Base

  # For Auditing and Versioning
  has_paper_trail

  default_scope -> { order(position: :asc) }

  belongs_to :publication

  validates :name, presence:true, length: {maximum: 30}
  validates :asset_id, presence:true
  validates :position, presence:true, numericality:true

  def to_s
    name
  end

end
