class Publication < ActiveRecord::Base
  # For Auditing and Versioning
  has_paper_trail

  # Allows to have type as a table column
  self.inheritance_column = nil

  # Types of publication
  enum type: [:preview, :live]

  # Every publication can have 0-many sections and ordered using position
  has_many :sections, -> {order(position: :asc)}, dependent: :destroy

  # The line is required by formtastic to have nested forms for models with has_many relationships
  accepts_nested_attributes_for :sections, allow_destroy: true

  # Length is kept shorter to faciliate dynamic url generation
  validates :name, presence:true, length: {maximum: 30}

  # Type of publication
  validates :type, presence:true

  # Check uniqueness of uri
  validates :uri, presence:true, uniqueness: true

  # Reload the routes if the user choses to edit the same
  after_save :reload_routes


  def to_s
    uri
  end

  private

    def reload_routes
      DynamicRouter.reload
    end

end
