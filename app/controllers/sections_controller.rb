class SectionsController < ApplicationController

  # custom sort method based on position determined by the drag and drop from UI
  def sort
    Rails.logger.info "Reordering sections #{params[:sections]}"
    Section.where(:publication_id => params[:publication_id]).each do |section|
        if position = params[:sections].index(section.id.to_s)
          section.update_attribute(:position, position+1) unless section.position == position + 1
        end
      end
      render :nothing => true, :status => 200
  end

end
