class PublicationsController < ApplicationController

  # method that converts publication object to json
  def show
    publication = Publication.find params[:id]
    render :json => publication, :only => [:name, :type, :uri],
      :include =>  [:sections => {:only => [:name, :asset_id, :color, :image] }]
  end

end
