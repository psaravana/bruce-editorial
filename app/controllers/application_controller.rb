class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Rescue from all 404 errors
  rescue_from ActionController::RoutingError, :with => :render_404

  # Add IP to the payload for logging
  def append_info_to_payload(payload)
    super
    payload[:ip] =  request.remote_ip
  end

  # Rescue from all 404 errors
  def render_404(exception = nil)
    puts "Rendering 404: #{exception.message}" if exception
    render json: { error: "#{exception.message}"}, status: :not_found
  end

  # Returns logged in user id or '1' (default id) for paper trail auditing
  def user_for_paper_trail
    admin_user_signed_in? ? current_admin_user.id : "1"
  end

end
