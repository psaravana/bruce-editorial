class ErrorsController < ApplicationController
  def not_found
    raise ActionController::RoutingError.new('API End Point Not found')
  end
end
