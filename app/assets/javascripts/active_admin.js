//= require active_admin/base
//= require select2
// require activeadmin-sortable

$(function(){

  var publicationType = $('#publication_type');

  //Function that updates the URI based on the name and type
  function updateURI(){
    var name = $('#publication_name').val().trim().split(' ').join('-'),
        type = publicationType.val().trim();
    $('#publication_uri').val((name!=='' && type!=='') ? ('/'+name.toLowerCase()+'/'+type) : '');
  };

  //For the select-boxes
  publicationType.select2({width: '225px'});
  //Event binding for publication type
  publicationType.on('change', updateURI);
  // Event binding for publication name
  $('#publication_name').on('keyup blur', updateURI);

});

(function($) {
  $(document).ready(function() {
    $('.handle').closest('tbody').activeAdminSortable();
  });

  $.fn.activeAdminSortable = function() {
    this.sortable({
      update: function(event, ui) {
        var url = ui.item.find('[data-sort-url]').data('sort-url');
        var data = $('.ui-sortable').sortable('toArray').map(function(elem){
          elem = elem.replace('section_','');
          return elem;
        });
        // console.log(data);
        $.ajax({
          url: '/sections/sort',
          type: 'post',
          data: { sections: data, publication_id: $('#publication').data('publication-id') },
          success: function() { window.location.reload() }
        });
      }
    });

    this.disableSelection();
  }
})(jQuery);
