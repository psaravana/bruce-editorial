class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      # A publication can have many sections
      t.belongs_to :publication

      t.string :name
      t.string :asset_id
      t.string :image
      t.string :color
      t.integer :position
      t.timestamps
    end
  end
end
