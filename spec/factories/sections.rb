FactoryGirl.define do
  # Sections
  factory :section do |f|
    f.sequence(:name) { |n| "#{Faker::Hacker.verb}-#{n}" }
    f.sequence(:asset_id) { |n|  "#{Faker::Lorem.characters(10)}#{n}" }
    f.sequence(:image){     Faker::Internet.url }
    f.sequence(:color) { |n| "#{Faker::Commerce.color}"}
    f.sequence(:position) {  Faker::Number.digit }
  end

end
