FactoryGirl.define do

  factory :publication do
    name Faker::Lorem.characters(3)
    type Publication.types.keys.shuffle.first
    uri  { "#{name}/#{type}" }

    # factory for has_many relationship
    # pub = FactoryGirl.create(:publication_with_sections)
    # pub = FactoryGirl.create(:publication_with_sections, sections_count: 2)
    factory :publication_with_sections do

      transient do
        sections_count 5
      end

      after(:create) do |publication, evaluator|
        create_list(:section, evaluator.sections_count, publication:publication)
      end

    end

  end

end
