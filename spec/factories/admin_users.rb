FactoryGirl.define do
  # Creates Editorial UI admin user
  factory :admin_user do |user|
    user.sequence(:email) { "admin@fairfaxmedia.com.au" }
    user.sequence(:password) { "password@123$" }
  end

end
