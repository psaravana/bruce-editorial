require 'spec_helper'

describe Publication do

  # Check if the factory setup is valid
  it "has a valid factory" do
    expect(build(:publication)).to be_valid
    expect(build(:publication_with_sections)).to be_valid
    expect(build(:publication_with_sections, :sections_count => 2)).to be_valid
  end


  it "is invalid without a name" do
    expect(build(:publication, name:nil)).to be_invalid
  end

  it "is valid without sections" do
    publication = build(:publication)
    expect(publication.sections.length).to eq(0)
    expect(publication).to be_valid
  end


end
