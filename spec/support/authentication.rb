
def sign_in_as_admin
    visit root_path

    user = create(:admin_user)
    fill_in('admin_user_email',     :with => user.email)
    fill_in('admin_user_password',  :with => user.password)

    click_button('Login')
end
