require 'spec_helper'

describe "Publications", :js => true do

    before(:each) do
      @publication = create(:publication_with_sections)
      sign_in_as_admin
      visit "/admin/publications/#{@publication.id}/sections"
    end

    before(:all) do
      Capybara.javascript_driver = :selenium
    end

  context "view publications with sections" do

    it "renders publcation along with sections" do

      # Check if publication details are in place
      expect(page).to have_content @publication.name
      expect(page).to have_content @publication.type
      expect(page).to have_content @publication.uri

      # Should have 6 rows for sections including the headers
      expect(page).to have_css("table#index_table_sections tr", :count => 6)
    end

    # custom drag_to method to test jquery-ui sortable
    def drag_to(source, target)
      builder = page.driver.browser.action
      source = source.native
      target = target.native

      builder.click_and_hold source
      builder.move_to        target, 1, 11
      builder.move_to        target
      builder.release        target
      builder.perform
    end


    it "shuffle sections within publication" do

      element_to_drag = find("tr.ui-sortable-handle:nth-of-type(1)").text

      # Get the handles and drag the first element to last
      handles = all(".handle")
      drag_to handles[0], handles[3]

      element_dragged =  find("tr.ui-sortable-handle:nth-of-type(4)").text
      expect(element_dragged).to eq(element_to_drag)

    end

  end


end
