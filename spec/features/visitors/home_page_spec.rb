require 'spec_helper'

# Feature: Home page
#   As a visitor
#   I want to visit a home page
#   So I can learn more about the website
feature 'Home page' do

  # Scenario: Visit the home page
  #   Given I am a visitor
  #   When I visit the home page
  #   Then I see "Editorial login to use the Editorial UI"
  scenario 'visit the home page' do
    visit root_path
    expect(page).to have_content 'Bruce Editorial Login'
  end

  scenario 'Invalid login' do
    visit root_path

    fill_in('admin_user_email',     :with => "invalid_username")
    fill_in('admin_user_password',  :with => "invalid_password")

    click_button('Login')

    expect(page).to have_content 'Invalid email or password'
  end

  scenario 'valid login' do
    sign_in_as_admin
    expect(page).to have_content 'Dashboard'
  end

end
