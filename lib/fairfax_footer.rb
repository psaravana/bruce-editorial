
class FairfaxFooter < ActiveAdmin::Component
  # Override build method to change the footer
  def build
    super(id: "footer")
    para "Copyright #{Date.today.year} Fairfax Media"
  end
end
