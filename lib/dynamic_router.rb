class DynamicRouter

  def self.load
    BruceEditorial::Application.routes.draw do

      if ActiveRecord::Base.connection.table_exists? 'publications'
        Publication.all.each do |publication|
          Rails.logger.info "Loading route #{publication.uri}"
          # Get is injected for URI in every publication
          get "#{publication.uri}", :to => "publications#show", defaults: {id: publication.id}
        end # End of publication
      end

    end # End of Application
  end

  def self.reload
    BruceEditorial::Application.routes_reloader.reload!
  end

end
