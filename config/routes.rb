Rails.application.routes.draw do

  # Handle all the 404 errors for the application
  get 'errors/not_found'

  # API end point for publications
  # get 'publications/show'
  # get 'publications/not_found'
  post 'sections/sort'


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root to: 'admin/dashboard#index'

  # Load all the dynamic routes
  DynamicRouter.load

  # Redirect to 404 for all the unknown routes. Dynamic routes needs to be loaded first
  get "*any", via: :all, to: "errors#not_found"

end
